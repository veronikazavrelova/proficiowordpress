<div class="bg-dark text-light">
<footer class="container pt-4 pt-md-5 pb-4 pb-md-5">
    <div class="text-center">
        <div class="row">
            <div class="col-12 col-md pb-3">
                <a class="navbar-brand" href="#">
                    <img src="<?php echo get_bloginfo('template_directory'); ?>/images/logo.svg">
                </a>
            </div>
            <div class="col-6 col-md">
                <h5>Resources</h5>
                <ul class="list-unstyled text-small">
                    <li><a class="text-muted" href="#">Resource</a></li>
                    <li><a class="text-muted" href="#">Resource name</a></li>
                    <li><a class="text-muted" href="#">Another resource</a></li>
                    <li><a class="text-muted" href="#">Final resource</a></li>
                </ul>
            </div>
            <div class="col-6 col-md">
                <h5>About</h5>
                <ul class="list-unstyled text-small">
                    <li><a class="text-muted" href="#">Team</a></li>
                    <li><a class="text-muted" href="#">Locations</a></li>
                    <li><a class="text-muted" href="#">Privacy</a></li>
                    <li><a class="text-muted" href="#">Terms</a></li>
                </ul>
            </div>
        </div>
    </div>
</footer>
</div>
